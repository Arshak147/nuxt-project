import Vue from 'vue'
import {ValidationProvider , ValidationObserver,extend} from 'vee-validate';
import {required , alpha , image , min} from 'vee-validate/dist/rules';

Vue.component('ValidationProvider' , ValidationProvider);
Vue.component('ValidationObserver' , ValidationObserver);

extend ('required' , {
    ...required,
    message : 'پر کردن این بخش الزامیست',
    
})

extend ('alpha', {
    ...alpha,
    message: 'از حروف استفاده کنید'
})
extend ('image' , {
    ...image,
    message : "فرمت انتخابی اشتباه است"
})
extend ('min' , {
    ...min,
    message : 'توضیحات بیشتری نیاز است'
})